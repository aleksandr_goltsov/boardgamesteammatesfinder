package com.boardgamesteammatesfinder.model;

import com.boardgamesteammatesfinder.web.rest.dto.UserDto;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.extern.slf4j.XSlf4j;

import javax.persistence.*;

@Data
@Entity
@Slf4j
@Table(name = "user_db")
@AllArgsConstructor
@NoArgsConstructor
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence", sequenceName = "sequence_generator", allocationSize = 1)
    @Column(name = "id")
    private Long id;
    @Column(name = "login")
    private String login;
    @Column(name = "password")
    private String password;

    public User(UserDto dto){
        login = dto.getLogin();
        password = dto.getPassword();
    }
}
