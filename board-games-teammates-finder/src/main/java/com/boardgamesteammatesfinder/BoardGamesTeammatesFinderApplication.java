package com.boardgamesteammatesfinder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BoardGamesTeammatesFinderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BoardGamesTeammatesFinderApplication.class, args);
	}

}
