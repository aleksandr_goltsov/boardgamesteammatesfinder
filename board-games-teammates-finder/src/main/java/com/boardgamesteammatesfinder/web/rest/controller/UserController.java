package com.boardgamesteammatesfinder.web.rest.controller;



import com.boardgamesteammatesfinder.model.User;
import com.boardgamesteammatesfinder.service.UserService;
import com.boardgamesteammatesfinder.web.rest.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/api/user")
public class UserController {

    private final UserService userService;

    @GetMapping
    public ResponseEntity<List<User>> getAllUsers() {

        List<User> list = userService.getAllUsers();
        if (list != null)
            return ResponseEntity.ok(list);

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
    @PostMapping
    public ResponseEntity<User> postUser(@RequestBody UserDto dto){

        User newUser = userService.postUser(dto);


        if (newUser != null) {
            return ResponseEntity.ok(newUser);
        }

        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(null);
    }
}
