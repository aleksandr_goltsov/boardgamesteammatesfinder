package com.boardgamesteammatesfinder.web.rest.dto;

import lombok.Data;

@Data
public class UserDto {
    private String login;
    private String password;
}
