package com.boardgamesteammatesfinder.service;

import com.boardgamesteammatesfinder.model.User;
import com.boardgamesteammatesfinder.repository.UserRepository;
import com.boardgamesteammatesfinder.web.rest.dto.UserDto;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public List<User> getAllUsers(){
        return userRepository.findAll();
    }

    public User postUser(UserDto dto){
        User newUser = new User(dto);
        return userRepository.save(newUser);
    }
}
